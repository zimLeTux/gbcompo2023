#include "sdk/hardware.h"
#include "sdk/video.h"
#include "sdk/oam.h"
#include "sdk/assets.h"
#include "sdk/joypad.h"
#include "sdk/interrupt.h"
#include <string.h>

ASSET(tiles, "tiles.2bpp");

uint8_t cursor_x = 0;
uint8_t cursor_y = 0;
uint8_t offset = 0;

#define SCREEN_WIDTH 160
#define SCREEN_HEIGHT 96

static const uint16_t bg_palettes[] = 
{
    PAL24(0xffcc22),
    PAL24(0xaacc22),
    PAL24(0x77cc22),
    PAL24(0x11cc22),
    
    PAL24(0xffcc22),
    PAL24(0xaacc22),
    PAL24(0x77cc22),
    PAL24(0x11cc22),
    
    PAL24(0xffcc22),
    PAL24(0xaacc22),
    PAL24(0x77cc22),
    PAL24(0x11cc22),
    
    PAL24(0xffcc22),
    PAL24(0xaacc22),
    PAL24(0x77cc22),
    PAL24(0x11cc22),
    
    PAL24(0xffcc22),
    PAL24(0xaacc22),
    PAL24(0x77cc22),
    PAL24(0x11cc22),
    
    PAL24(0xffcc22),
    PAL24(0xaacc22),
    PAL24(0x77cc22),
    PAL24(0x11cc22),
    
    PAL24(0x223116),
    PAL24(0xaacc22),
    PAL24(0x77cc22),
    PAL24(0x11cc22),
    
    PAL24(0xffcc22),
    PAL24(0xaacc22),
    PAL24(0x77cc22),
    PAL24(0x11cc22)
};

void main() {
    //Load our initial vram (this is slow, but always works, even outside of vblank)
    vram_memcpy(0x8000, tiles, tiles_end - tiles);
    vram_memcpy(0x9000, tiles, tiles_end - tiles);
    //Setup the OAM for sprite drawing
    oam_init();

    //Set some default DMG palette data
    rBGP = 0b11100100;
    rOBP0 = 0b11100100;
    rOBP1 = 0b11100100;

    //Put some sprites on the screen
    shadow_oam[0].y = 0x20;
    shadow_oam[0].x = 0x20;
    shadow_oam[0].tile = 0x04;
    shadow_oam[0].attr = 0x00;
    shadow_oam[1].y = 0x24;
    shadow_oam[1].x = 0x24;
    shadow_oam[1].tile = 0x02;
    shadow_oam[1].attr = 0x00;

    //Make sure sprites and the background are drawn
    rLCDC = LCDC_ON | LCDC_OBJON | LCDC_BGON;

    //Setup the VBLANK interrupt, but we don't actually enable interrupt handling.
    // We only do this, so HALT waits for VBLANK.
    rIF = 0;
    rIE = IE_VBLANK;
    
    HALT();
    rIF = 0;
    cgb_background_palette(bg_palettes);
    
    HALT();
    rIF = 0;

    vram_memset(0x9800, 0x80, 32 * 32);
    rVBK = 1;
    vram_memset(0x9800, 0x06, 32 * 32);
    rVBK = 0;
    
    uint8_t i = 0;
    for(uint8_t x=0; x<(SCREEN_WIDTH/8); x++)
    {
        for(uint8_t y=0; y<(SCREEN_HEIGHT/8)/2; y++)
        {
            vram_set(0x9800 + x + y * 0x20, i++);
            rVBK = 0x01;
            vram_set(0x9800 + x + y * 0x20, 0b00000000);
            rVBK = 0x00;
        }
    }
    i = 0;
    for(uint8_t x=0; x<(SCREEN_WIDTH/8); x++)
    {
        for(uint8_t y=(SCREEN_HEIGHT/8)-1; y>=(SCREEN_HEIGHT/8)/2; y--)
        {
            vram_set(0x9800 + x + y * 0x20, i++);
            rVBK = 0x01;
            vram_set(0x9800 + x + y * 0x20, 0b01000001);
            rVBK = 0x00;
        }
    }
    
    /*for (uint8_t j = 0; j < SCREEN_WIDTH/8; j++)
    {
        for (uint8_t i = 0; i < SCREEN_HEIGHT/2; i++)
        {
            vram_set(0x9000 + j*2*8 + i*2, 0xcc);
        }
    }*/
    //memset((void*)0xC100, 0x00, 2*(SCREEN_WIDTH/8)*(SCREEN_HEIGHT/2));
    
    //rSCX = 0xff-18;
    rSCX = 0;
    rSCY = 0xff-16;
    

    while(1) {
        joypad_update();
        if (joypad_state & PAD_LEFT) cursor_x --;
        if (joypad_state & PAD_RIGHT) cursor_x ++;
        if (joypad_state & PAD_UP) cursor_y --;
        if (joypad_state & PAD_DOWN) cursor_y ++;
        if (joypad_state & (PAD_A | PAD_B)) {
            offset++;
        }
        memset((void*)0xC100, offset, 2*(SCREEN_WIDTH/8)*(SCREEN_HEIGHT/2));

        // Screen to OAM coordinates
        shadow_oam[0].y = cursor_y + 16;
        shadow_oam[0].x = cursor_x + 8;
        vram_memcpy(0x9000, (void*)0xC100, 2*(SCREEN_WIDTH/8)*(SCREEN_HEIGHT/2));

        //Wait for VBLANK
        HALT();
        rIF = 0;    //As global interrupts are not enabled, we need to clear the interrupt flag.

        //Copy the sprites into OAM memory
        oam_dma_copy();
    }
}
