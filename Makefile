# Name of your project, will set the name of your ROM.
PROJECT_NAME := GBCOMPO2023
# Run "rgbfix --mbc-type help" for possible MBC types
MBC     := MBC5+RAM+BATTERY
# Target should be a combination of DMG, CGB and SGB
TARGETS := CGB

include gbsdk/rules.mk
